UPDATE
   `transactions`
    INNER JOIN`invoices` 
        ON (`transactions`.`ref_no` = `invoices`.`id`)
    INNER JOIN`customers` 
        ON (`invoices`.`customer` = `customers`.`name`)
        SET transactions.`entity_id`=`customers`.id
WHERE (`transactions`.`entity_id` =9999
    AND `transactions`.`type` ='sales' );
UPDATE
   `customer_ledgers`
    INNER JOIN`invoices` 
        ON (`customer_ledgers`.`ref_no` LIKE CONCAT('%',`invoices`.`id`))
    INNER JOIN`customers` 
        ON (`invoices`.`customer` = `customers`.`name`)
       SET customer_ledgers.`customer_id`= customers.id
WHERE (`customer_ledgers`.`customer_id` =9999 );
UPDATE
  customers 
  INNER JOIN 
    (SELECT 
      customer_id,
      SUM(amount) AS credit_amount 
    FROM
      customer_ledgers 
    WHERE TIMESTAMP >= '2017-02-08 12:00:00' 
      AND TIMESTAMP <= '2017-02-15 10:00:00' 
      AND flag = 'c' 
    GROUP BY customer_id) AS credit 
    ON (
      credit.customer_id = customers.id
    )
    SET customers.`current_balance` = customers.`current_balance` +credit.credit_amount;