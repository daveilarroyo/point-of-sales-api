<?php
	require ('formsheet.php');
	class ReportSheet extends Formsheet{
		function createSheet(){
			$business_name = Configure::read('Business.name');
			$business_addr = Configure::read('Business.address');
			$business_cont = Configure::read('Business.contact');
			parent::createSheet();
			$this->showLines = false;
			$this->createGrid(0,0,$this->_width,$this->_height,25,15);
			$this->GRID['font_size']=18;
			$this->centerText(0,1.5,$business_name,15,'b');
			$this->GRID['font_size']=12;
			$this->centerText(0,2,$business_addr,15);
			$this->centerText(0,2.5,$business_cont,15);
		}
	}
?>