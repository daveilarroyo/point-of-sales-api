<?php
	require ('reportsheet.php');
	class SOA extends ReportSheet{
		function __construct(){
			$this->_height=11;
			parent::__construct();
		}
		function info($info){
			$this->GRID['font_size']=10;
			$this->leftText(1,3.5,'Account Name:',null,'b');
			$this->leftText(3,3.5,$info['account'],null);
			$this->leftText(1,4,'Billing Period:',null,'b');
			$this->leftText(3,4,$info['detail']['cutoff'].', '.date('Y',time()),null);
			$this->leftText(10.25,3.5,'Date:',null,'b');
			$this->leftText(11.75,3.5,$info['date'],null);
			$this->leftText(10.25,4,'Due Date:',null,'b');
			$this->leftText(11.75,4,$info['due_date'],null);
		}
		function details($details,$info){
			$this->centerText(1,5,'No.',2,'b');
			$this->centerText(3,5,'Date',3,'b');
			$this->centerText(6,5,'Details',5,'b');
			$this->centerText(11,5,'Amount',3,'b');
			$y=5.55;
			$pages = array_chunk($details,30);
			$RUN_TOTAL = 0;
			foreach($pages as $page_no => $page){
				foreach($page as $detail){
					$this->centerText(1,$y,$detail['no'],2);
					$this->centerText(3,$y,$detail['date'],3);
					$this->centerText(6,$y,$detail['details'],5);
					$this->rightText(10,$y,$detail['amount']['text'],3);
					$y+=0.5;
					$RUN_TOTAL+=$detail['amount']['value'];
				}
				$this->rightText(10,22,'Page '.($page_no+1).' of '.count($pages),3);
				if($page_no+1<count($pages)){
					$y-=0.25;
					$this->DrawLine($y,'h',array(11.5,2));
					$y+=0.45;
					$this->rightText(8.5,$y,'TOTAL',3,'b');
					$DISP_TOTAL = number_format(abs($RUN_TOTAL),2,'.',',');
					if($RUN_TOTAL<0){
						$DISP_TOTAL = '('.$DISP_TOTAL.')';
					}
					$this->rightText(10,$y,$DISP_TOTAL,3,'b');
					$this->createSheet();
					$this->info($info);
					$y=5.55;
					$this->centerText(1,5,'No.',2,'b');
					$this->centerText(3,5,'Date',3,'b');
					$this->centerText(6,5,'Details',5,'b');
					$this->centerText(11,5,'Amount',3,'b');
					$this->centerText(1,$y,'****** BALANCE FORWARDED *****',10);
					$this->rightText(10,$y,$DISP_TOTAL,3);
					$y+=0.5;
				}
			}
			
			$y-=0.25;
			$this->DrawLine($y,'h',array(11.5,2));
			$y+=0.45;
			$this->rightText(8.5,$y,'TOTAL',3,'b');
			$this->rightText(10,$y,$info['total'],3,'b');
		}
		function footnote($info){
			$this->GRID['font_size']=15;
			$this->leftText(1,21,'GRAND TOTAL',4,'b');
			$this->rightText(5,21,$info['total'],3,'b');
			$this->GRID['font_size']=10;
			$this->leftText(1,23,'Prepared by:',4);
			$this->leftText(2.5,23,$info['user'],4);
			$this->rightText(10,23,'Received by:',1);
			$this->DrawLine(23,'h',array(11.25,2));
			$this->rightText(10,23.5,'Date:',1);
			$this->DrawLine(23.5,'h',array(11.25,2));
		}
	}
?>