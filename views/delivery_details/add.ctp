<div class="deliveryDetails form">
<?php echo $this->Form->create('DeliveryDetail');?>
	<fieldset>
		<legend><?php __('Add Delivery Detail'); ?></legend>
	<?php
		echo $this->Form->input('product_id');
		echo $this->Form->input('price');
		echo $this->Form->input('quantity');
		echo $this->Form->input('amount');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Delivery Details', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Products', true), array('controller' => 'products', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Product', true), array('controller' => 'products', 'action' => 'add')); ?> </li>
	</ul>
</div>