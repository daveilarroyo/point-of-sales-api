<?php echo $this->Html->script(array('invoice'),array('inline'=>false));  ?>
<div class="invoices form" ng-controller="InvoiceController">
<?php echo $this->Form->create('Invoice');?>
	<fieldset>
		<legend><?php __('Add Invoice'); ?></legend>
	<?php
		echo $this->Form->input('Invoice.invoice_date');
		echo $this->Form->input('Invoice.customer');
		echo $this->Form->input('Invoice.total',array('type'=>'hidden'));
	?>
	<div class="input text">
		<label for="InvoiceTotal">Total</label>
		<h3 class="total">{{total  | currency : ""}}</h3>
	</div>
	<table cellpadding="0" cellspacing="0">
		<tr>
			<th>Item</th>
			<th>Price</th>
			<th>Qty</th>
			<th>Amount</th>
		</tr>
		<tr ng-repeat="item in items"> 
			<td>{{item.particular}}</td>
			<td> {{item.srp  | currency : ""}}</td>
			<td> {{item.quantity}}</td>
			<td> {{item.amount | currency : ""}}</td>
			<td ng-init="total = total + item.amount;">
			<a href="#" ng-click="removeItem($index)">X</a>
			<input type="hidden" name="data[InvoiceDetail][{{$index}}][product_id]" value="{{item.product_id}}"/>
			<input type="hidden" name="data[InvoiceDetail][{{$index}}][quantity]" value="{{item.quantity}}"/>
			<input type="hidden" name="data[InvoiceDetail][{{$index}}][price]" value="{{item.srp}}"/>
			<input type="hidden" name="data[InvoiceDetail][{{$index}}][amount]" value="{{item.amount}}"/>
			</td>
		</tr>
	</table>
	</fieldset>
	<div class="input select">
	<select id="InvoiceDetailProductId" ng-model="selectedProduct">
		 <option ng-repeat="product in products" value="{{product.id}}">{{product.particular}}</option>
	</select>
	</div>
	<div class="input text">
	<input type="numeric" placeholder="quantity" ng-model="quantity"/>
	</div>
	<div class="submit inline"><input type="button" value="Add" ng-click="addItem()"></div>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Invoices', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Invoice Details', true), array('controller' => 'invoice_details', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Invoice Detail', true), array('controller' => 'invoice_details', 'action' => 'add')); ?> </li>
	</ul>
</div>