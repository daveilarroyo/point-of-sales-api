<div class="products form">
<?php echo $this->Form->create('Product');?>
	<fieldset>
		<legend><?php __('Edit Product'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('category_id');
		echo $this->Form->input('particular');
		echo $this->Form->input('part_no');
		echo $this->Form->input('description');
		echo $this->Form->input('quantity');
		echo $this->Form->input('capital');
		echo $this->Form->input('srp');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('Product.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('Product.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Products', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Categories', true), array('controller' => 'categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Category', true), array('controller' => 'categories', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Delivery Details', true), array('controller' => 'delivery_details', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Delivery Detail', true), array('controller' => 'delivery_details', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Invoice Details', true), array('controller' => 'invoice_details', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Invoice Detail', true), array('controller' => 'invoice_details', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Order Details', true), array('controller' => 'order_details', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Order Detail', true), array('controller' => 'order_details', 'action' => 'add')); ?> </li>
	</ul>
</div>