<div class="deliveries form">
<?php echo $this->Form->create('Delivery');?>
	<fieldset>
		<legend><?php __('Add Delivery'); ?></legend>
	<?php
		echo $this->Form->input('order_no');
		echo $this->Form->input('delivery_date');
		echo $this->Form->input('total');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Deliveries', true), array('action' => 'index'));?></li>
	</ul>
</div>