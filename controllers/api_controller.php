<?php
class ApiController extends AppController {
	var $uses = null;
	function authenticate(){
		$data = $_POST['data'];
		$User = &ClassRegistry::init('User');
		$this->Session->write('USERID',null);
		$this->Session->write('USERNAME',null);
		if($user = $User->authenticate($data)){
			$response = array();
			$response['status'] = '00';
			$response['message'] = 'User authenticated';
			$response['response'] = $user;
			$this->Session->write('USERID',$user['User']['id']);
			$this->Session->write('USERNAME',$user['User']['username']);
		}else{
			$response = array();
			$response['status'] = '404';
			$response['message'] = 'Invalid Credentials';
		}
		$this->header('Content-Type: application/json');
		echo json_encode($response,JSON_NUMERIC_CHECK );exit;
	}
	function logout(){
		$this->Session->write('USERID',null);
		$this->Session->write('USERNAME',null);
		$response = array();
		$response['status'] = '00';
		$response['message'] = 'User logged out';
		$this->header('Content-Type: application/json');
		echo json_encode($response,JSON_NUMERIC_CHECK );exit;
	}
	function renew(){
		$response['status'] = '00';
		$response['message'] = 'Session Renewed';
		$this->header('Content-Type: application/json');
		echo json_encode($response,JSON_NUMERIC_CHECK );exit;
	}
	function index($endpoint){
		date_default_timezone_set("Asia/Manila");
		$__Class = Inflector::classify($endpoint);
		$Endpoint = &ClassRegistry::init($__Class);
		$format = (isset($_GET['format']))?$_GET['format']:'json';
		if($this->RequestHandler->isGet()){
			$type='all';
			$Endpoint->recursive=$__Class=='Transaction'||$__Class=='CustomerLedger'||$__Class=='SupplierLedger' ||$__Class=='User'?2:-1;
			$data = array();
			$conf =array();
			$conf['conditions']=array();
			if(!empty($_GET['data'])){
				$this->data =  $_GET['data'];
				if(isset($this->data['id'])||isset($this->data['ref_no'])){
					$type='first';
					if($__Class=='Transaction'){
						if(isset($this->data['type'])){
							$conf['conditions']=array(
									$__Class.'.ref_no'=>$this->data['ref_no'],
									$__Class.'.type'=>$this->data['type'],
								);
						}
					}
					else $conf['conditions']=array($__Class.'.id'=>$this->data['id']);
				}
			}else{
				$page = isset($_GET['page'])?$_GET['page']:1;
				if($format=='csv'||$format=='pdf'&&!isset($_GET['page'])){
					$page = null;
				}
				$limit = $conf['limit'] = isset($_GET['limit'])?$_GET['limit']:500;
				if($format=='csv'||$format=='pdf'&&!isset($_GET['limit'])){
					 $conf['limit'] = $limit = null;
				}
				$offset = $conf['offset'] = $page&&$limit?($page-1)*$limit:null;
				$keyword  = null;
				$fields = null;
				$filter = null;
				$export = null;
				$soa  = null;
				$last_bill  = null;
				$dashboard = null;
				if(isset($_GET['sort'])) {
					switch($_GET['sort']){
						case 'latest':
							if($__Class=='Transaction'||$__Class=='CustomerLedger'||$__Class=='SupplierLedger')
								$conf['order']=array($__Class.'.timestamp DESC');
							else
								$conf['order']=array($__Class.'.modified DESC');
						break;
						case 'oldest':
							if($__Class=='Transaction'||$__Class=='CustomerLedger'||$__Class=='SupplierLedger')
								$conf['order']=array($__Class.'.timestamp ASC');
							else
								$conf['order']=array($__Class.'.modified ASC');
						break;
					}
				}
				if(isset($_GET['keyword'])) $keyword = '%'.$_GET['keyword'].'%';
				//pr($_GET);exit;
				if($keyword&&isset($_GET['fields'])) $fields = explode(',',$_GET['fields']);
				if(isset($_GET['export'])) $export = explode(',',$_GET['export']);
				if($keyword && $fields){
					$cond = array();
					foreach($fields as $fld){
						if($fld=='entity_name'){
							$cond['OR'] = array();
							if($__Class!='SupplierLedger') $cond['OR']['Customer.name LIKE'] = $keyword;
							if($__Class!='CustomerLedger') $cond['OR']['Supplier.name LIKE'] = $keyword;
						}
						else $cond[$__Class.'.'.$fld.' LIKE'] = $keyword;
					}
					$conf['conditions'] = array_merge($conf['conditions'],array('OR'=>$cond));
				}
				if(isset($_GET['filter'])) $filter = json_decode($_GET['filter'],true);
				if(isset($_GET['soa'])) $soa =true;
				if(isset($_GET['last_bill'])) $last_bill =$_GET['last_bill'];
				if(isset($_GET['dashboard'])) $dashboard =$_GET['dashboard'];
				if($filter){
					$__filter = array();
					foreach($filter as $key=>$value){
						switch($__Class){
							case 'Product':
								switch($key){
									case 'category':
										if($value!='ALL') $__filter['Product.category_id'] = $value;
									break;
									case 'quantity':
										switch($value){
											case 'ZERO':
												array_push($__filter,'Product.soh_quantity = 0');
											break;
											case 'MIN':
												 array_push($__filter,'Product.soh_quantity < Product.min_quantity');
											break;
											case 'MAX':
												array_push($__filter,'Product.soh_quantity > Product.max_quantity');
											break;
											case 'ADJ':
												array_push($__filter,array('OR'=>array('Product.tmp_quantity','Product.tmp_srp')));
											break;
										}
									break;
									default:
										array_push($__filter,'Product.'.$key.' = \''.$value.'\'');
									break;
								}
							break;
							default:
								switch($key){
									case 'from':
										$__filter[$__Class.'.timestamp >='] = $value. ' 00:00:00';
									break;
									case 'to':
										$__filter[$__Class.'.timestamp <='] = $value. ' 23:59:59';
									break;
									case 'coverage':
										$today = date('Y-m-d', strtotime('0 days'));
										
										if($__Class=='Transaction'||$__Class=='CustomerLedger'||$__Class=='SupplierLedger')
											$key = 'timestamp';
										else
											$key = 'created';
										switch($value){
											case 'today':
												$__filter[$__Class.'.'.$key.' >='] = $today. ' 00:00:00';
												$__filter[$__Class.'.'.$key.' <='] = $today. ' 23:59:59';
											break;
											case '7D':
												$lastWeek = date('Y-m-d', strtotime('-7 days'));
												$__filter[$__Class.'.'.$key.' >='] = $lastWeek. ' 00:00:00';
												$__filter[$__Class.'.'.$key.' <='] = $today. ' 23:59:59';
											break;
											case '30D':
												$lastMonth = date('Y-m-d', strtotime('-30 days'));
												$__filter[$__Class.'.'.$key.' >='] = $lastMonth. ' 00:00:00';
												$__filter[$__Class.'.'.$key.' <='] = $today. ' 23:59:59';
											break;
											case 'YTD':
												$lastMonth = date('Y-m-d', strtotime('-365 days'));
												$__filter[$__Class.'.'.$key.' >='] = $lastMonth. ' 00:00:00';
												$__filter[$__Class.'.'.$key.' <='] = $today. ' 23:59:59';
											break;
											case 'SOA':
												$bill_month = 'this';
												$current_month = (int) date('m', strtotime("this month"));
												if($last_bill &&  $last_bill == $current_month) $bill_month = "this";
												$firstDay = date('2016-m-d', strtotime("first day of $bill_month month"));
												$lastDay = date('2016-m-d', strtotime("last day of $bill_month  month"));
												$__filter[$__Class.'.'.$key.' >='] = $firstDay. ' 00:00:00';
												$__filter[$__Class.'.'.$key.' <='] = $lastDay. ' 23:59:59';
											break;
										}
									break;
									default:
										if($key=='type'&&($__Class=='CustomerLedger'||$__Class=='SupplierLedger')) {
											if($value!='ALL') $__filter[$__Class.'.ref_no LIKE'] = $value.'%';
										}else if($key=='type'&&$value=='ALL'&&$__Class=='Transaction'){
											
										}else $__filter[$__Class.'.'.$key] = explode(',',$value);
									break;
								}
							break;
						}
					}
					
					if(count($__filter)) $conf['conditions'] = array_merge($conf['conditions'],array('AND'=>$__filter));
				}
				
				if($export){
					$conf['fields']=$export;
				}
				if($dashboard){
					$Endpoint->recursive = 0;
					switch($dashboard){
						case 'day':
							$conf['group'] = array('HOUR(Transaction.timestamp)');
							$conf['fields'] = array('SUM(Transaction.amount) as dash_amount','CONCAT(HOUR(Transaction.timestamp),":00") as dash_time');
						break;
						case 'week':
							$conf['group'] = array('DAY(Transaction.timestamp)');
							$conf['fields'] = array('SUM(Transaction.amount) as dash_amount','CONCAT(YEAR(Transaction.timestamp),"-",MONTH(Transaction.timestamp),"-", DAY(Transaction.timestamp) ) as dash_date');
						break;
						case 'month':
							$conf['group'] = array('MONTH(Transaction.timestamp)','DAY(Transaction.timestamp)');
							$conf['fields'] = array('SUM(Transaction.amount) as dash_amount','MONTH(Transaction.timestamp) as dash_month','CONCAT(YEAR(Transaction.timestamp),"-",MONTH(Transaction.timestamp),"-", DAY(Transaction.timestamp) ) as dash_date');
						break;
						case 'year':
							$conf['group'] = array('MONTH(Transaction.timestamp)');
							$conf['fields'] = array('SUM(Transaction.amount) as dash_amount','CONCAT(YEAR(Transaction.timestamp),"-",MONTH(Transaction.timestamp),"-", DAY(Transaction.timestamp) ) as dash_date');
						break;
					}
					
				}
				$count_conf = $conf;
				unset($count_conf['limit']);
				unset($count_conf['offset']);
				$count = $Endpoint->find('count',$count_conf);
				$last = $limit?ceil($count/$limit):1;
				$next = $page < $last ? $page + 1:null;
				$prev = $page>1?$page - 1:null;
			}
			$meta = array();
			$base_url = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'];
			$href = $base_url.$_SERVER['REQUEST_URI'];
			$meta['href'] = $href;

			if($type=='all'){
				$get_vars = array();
				foreach($_GET as $get_key=>$get_val){
					if($get_key!='url'&&$get_key!='page'){
						array_push($get_vars,$get_key.'='.$get_val);
					}
				}
				$path = parse_url($href, PHP_URL_PATH);
				$page_url = '/'.$endpoint.'?'.implode('&',$get_vars).'&page=';
				if(!$dashboard){
					$meta['next'] = $next? $page_url.$next:null;
					$meta['prev'] = $prev? $page_url.$prev:null;
					$meta['last'] = $page_url.$last;
					$meta['items'] = $count;
					$meta['pages'] = $last;
				}else{
					foreach($filter as $key=>$value)
						$meta[$key]=$value;
				}
				if($dashboard){
					$dash_prev_month =  $dash_curr_month = null;
					$dash_prev_year =  $dash_curr_year = null;
					$dash_coverage = $dashboard=='day';
					$dash_year =  $filter['coverage']=='YTD';
				}
				
				foreach($Endpoint->find($type,$conf) as $ei=>$ep){
					array_push($data,$ep[$__Class]);
					if($Endpoint->recursive==2 || $dashboard){
						foreach($ep as $epk => $epv){
							if($epk=='Supplier' && $__Class=='Transaction'){
								unset($epv['Delivery']);
								unset($epv['Order']);
							}
							if($dashboard &&  $epk=='0' ){
								$data[$ei]['amount']= $epv['dash_amount'];
								if($dash_year){
									$dash_time =  strtotime($epv['dash_date']);
									$dash_curr_year = date('Y',$dash_time);
									$data[$ei]['date']= date($dash_curr_year!=$dash_prev_year?'M Y':'M', $dash_time);
									$dash_prev_year = $dash_curr_year;	
								}else{
									$dash_time =  strtotime($epv[$dash_coverage?'dash_time':'dash_date']);
									$dash_curr_month = date('M',$dash_time);
									$data[$ei]['date']= date($dash_coverage?'h A':($dash_curr_month!=$dash_prev_month?'M d':'d'), $dash_time);
									$dash_prev_month = $dash_curr_month;	
								}
										
							} 
							else if($epk!=$__Class) $data[$ei][$epk]=$epv;
						}
					}
				}
			}else{
				$result = $Endpoint->find($type,$conf);
				$data = $result[$__Class];
				switch($__Class){
					case 'User':
						$access = array();
						foreach($result['Module'] as $module){
							unset($module['ModuleUser']);
							array_push($access,$module);
						}
						unset($data['password']);
						$data['access'] = $access;
					break;
					case 'Product':
						$InventoryAdjustment = &ClassRegistry::init('InventoryAdjustment');
						$InventoryLog = &ClassRegistry::init('InventoryLog');
						$adjustment = $data['tmp_quantity']?$InventoryAdjustment->getLastAdjustment($data['id']):false;
						if($adjustment){
							$adjustment =  $adjustment[0]['inventory_adjustments'];
							$logs = $InventoryLog->getProductActivity($data['id'],$adjustment['created']);
							$adj_quantity =  $adjustment['tmp_quantity'];
							foreach($logs as $log){
								switch($log['inventory_logs']['source']){
									case 'POS': case 'R2S': case 'CDL': case 'CRC': case 'CTI':
										$adj_quantity-=$log[0]['quantity'];
									break;
									case 'DEL': case 'TIN':  case 'RFC':  case 'CSS': case 'CRS':
										$adj_quantity+=$log[0]['quantity'];
									break;									
								}
							}
							$adjustment['adj_quantity'] = $adj_quantity;
							$adjustment['adj_qty_timestamp'] =date('M d Y h:i a ',strtotime($adjustment['created']));
						}else{
							$adjustment = null;
							$logs = null;
						}
						if($data['tmp_srp']){
							if(!$adjustment) $adjustment = array();
							$adjustment['tmp_srp']  = $data['tmp_srp'];
							$adjustment['adj_srp_timestamp']  = date('M d Y h:i a ',strtotime($data['created']));
						}
						$data['adjustment']=$adjustment;
						$data['logs']=$logs;
					break;
					case 'Transaction':
						foreach($result as $key=>$value){
							if($key!='Transaction'){
								$data[$key] = $value;
							}
						}
					break;
					case 'Customer': case 'Supplier':
						$Entity = &ClassRegistry::init($__Class);
						$Ledger = &ClassRegistry::init($__Class.'Ledger');
						$entity = $Entity->findById($data['id']);
						$entity  = $entity [$__Class];
						$bill_month = 'this';
						$current_month = (int) date('m', strtotime("this month"));
						if($entity['last_bill'] &&  $entity['last_bill'] == $current_month) $bill_month = "this";
						foreach($Ledger->getBalances($data['begin_balance'],$data['id'],$bill_month) as $key=>$value){
							$data[$key] = $value;
						}
						$data['allow_posting'] = ($entity['last_bill'] < $current_month || ($current_month==1 && $entity['last_bill']!=1) ||  abs($current_month - $entity['last_bill']) > 1  )  && $entity['status']=='open';
					break;
				}
			}
		}else if($this->RequestHandler->isPost()){
			$data = array();
			$meta = array();
			if(!empty($_POST['data'])){
				$this->data =  $_POST['data'];
				$this->data['header']['user'] =  $this->Session->read('USERNAME');
				$action = 'save';
				if(isset($this->data['action'])) $action = $this->data['action'];
				switch($action){
					case 'save':
						if($__Class=='Transaction'){
							$this->data['header']['timestamp'] = $this->data['header']['date']. date(' H:i:s', time());
							switch($this->data['header']['type']){
								case 'sales': 
									$Invoice = &ClassRegistry::init('Invoice');
									if(isset($this->data['header']['customer']['name'])){
										$this->data['header']['customer'] = $this->data['header']['customer']['name'];
									}
									$Invoice->save($this->data['header']);									
									$invoice_id = $Invoice->id;
									$this->data['header']['ref_no'] = $invoice_id;
									
									$Endpoint->save($this->data['header']);
									$transaction_id = $Endpoint->id;
									
									$InvoiceDetail = &ClassRegistry::init('InvoiceDetail');
									
									foreach($this->data['details'] as $detail){
										$detail['id']=null;
										$detail['soh_quantity'] = $detail['quantity'];
										$detail['invoice_id']=$invoice_id;
										$detail['transaction_id']=$transaction_id;
										$InvoiceDetail->save($detail);
										$InvoiceDetail->Product->updateInventory(array($detail),-1,'POS');
										$InvoiceDetail->Product->updatePricing(array($detail),'tmp');
										$Endpoint->TransactionDetail->save($detail);
									}
									$InvoicePayment = &ClassRegistry::init('InvoicePayment');
									foreach($this->data['payments'] as $payment){
										$payment['id']=null;
										$payment['invoice_id']=$invoice_id;
										$payment['transaction_id']=$transaction_id;
										$InvoicePayment->save($payment);
										$Endpoint->TransactionPayment->save($payment);
										if($payment['payment_type']=='CHRG'){
											switch($this->data['header']['entity_type']){
												case 'customer':
													$CustomerLedger = &ClassRegistry::init('CustomerLedger');
													$entry = array('CustomerLedger'=>array(
																'id'=>null,
																'customer_id'=>$this->data['header']['entity']['id'],
																'ref_no'=>$this->data['header']['type'].'-'.$this->data['header']['ref_no'],
																'particulars'=>$payment['detail'],
																'amount'=>$payment['amount'],
																'flag'=>$this->data['header']['type']=='sales'?'c':'d',
																'timestamp'=>$this->data['header']['timestamp'],
															));
													$CustomerLedger->save($entry);
												break;
											}
										}else if($payment['payment_type']=='CASH'){
											$CashFlow =  &ClassRegistry::init('CashFlow');
											$entry = array('CashFlow'=>array(
															'id'=>null,
															'particulars'=>'sales-'.$invoice_id,
															'amount'=>$this->data['header']['amount'],
															'flag'=>'c',//Income
															'timestamp'=>$this->data['header']['timestamp']

														));
											$CashFlow->save($entry);
										}
									}
								break;
								case 'deliveries':
									$Delivery = &ClassRegistry::init('Delivery');
									if(isset($this->data['header']['supplier']['name'])){
										$this->data['header']['supplier'] = $this->data['header']['supplier']['name'];
									}
									$Delivery->save($this->data['header']);									
									$delivery_id = $Delivery->id;
									$this->data['header']['ref_no'] = $delivery_id;
									
									$Endpoint->save($this->data['header']);
									$transaction_id = $Endpoint->id;
									
									$DeliveryDetail = &ClassRegistry::init('DeliveryDetail');
									
									foreach($this->data['details'] as $detail){
										$detail['id']=null;
										$detail['price'] = $detail['capital'];
										$detail['soh_quantity'] = $detail['quantity'] = $detail['delivered'];
										$detail['delivery_id']=$delivery_id;
										$detail['transaction_id']=$transaction_id;
										$DeliveryDetail->save($detail);
										$DeliveryDetail->Product->updateInventory(array($detail),1,'DEL');
										$DeliveryDetail->Product->updatePricing(array($detail),'delivery',$delivery_id);
										$Endpoint->TransactionDetail->save($detail);
									}
									
									$DeliveryPayment = &ClassRegistry::init('DeliveryPayment');
									
									foreach($this->data['payments'] as $payment){
										$payment['id']=null;
										$payment['delivery_id']=$delivery_id;
										$payment['transaction_id']=$transaction_id;
										$DeliveryPayment->save($payment);
										$Endpoint->TransactionPayment->save($payment);
										if($payment['payment_type']=='CHRG'){
											switch($this->data['header']['entity_type']){
												case 'supplier':
													$SupplierLedger = &ClassRegistry::init('SupplierLedger');
													$entry = array('SupplierLedger'=>array(
																'id'=>null,
																'supplier_id'=>$this->data['header']['entity']['id'],
																'ref_no'=>$this->data['header']['type'].'-'.$this->data['header']['ref_no'],
																'particulars'=>$payment['detail'],
																'amount'=>$payment['amount'],
																'flag'=>$this->data['header']['type']=='deliveries'?'c':'d',
																'timestamp'=>$this->data['header']['timestamp'],
															));
													$SupplierLedger->save($entry);
												break;
											}
										}
									}
									if(isset($this->data['header']['doc_no'])){
										$doc_no = $this->data['header']['doc_no'];
										$delivery_date = $this->data['header']['delivery_date'];

										$Order = &ClassRegistry::init('Order');
										$order_trnx = $Endpoint->find('first',array('conditions'=>array('type'=>'orders','ref_no'=>$doc_no)));
										$trnx_id = $order_trnx[$__Class]['id'];

										$Order->save(array('id'=>$doc_no,'status'=>'fulfilled','delivery_date'=>$delivery_date));
										$Endpoint->save(array('id'=>$trnx_id,'status'=>'fulfilled'));
									}
									
								break;
								case 'orders':
									$Order = &ClassRegistry::init('Order');
									if(isset($this->data['header']['supplier']['name'])){
										$this->data['header']['supplier'] = $this->data['header']['supplier']['name'];
									}
									$Order->save($this->data['header']);									
									$order_id = $Order->id;
									$this->data['header']['ref_no'] = $order_id;
									
									$Endpoint->save($this->data['header']);
									$transaction_id = $Endpoint->id;
									
									$OrderDetail = &ClassRegistry::init('OrderDetail');
									
									foreach($this->data['details'] as $detail){
										$detail['id']=null;
										$detail['price'] = $detail['capital'];
										$detail['order_id']=$order_id;
										$detail['transaction_id']=$transaction_id;
										$OrderDetail->save($detail);
										$Endpoint->TransactionDetail->save($detail);
									}
								break;
								case 'tradein': case 'return':
									$Endpoint->save($this->data['header']);
									$transaction_id = $Endpoint->id;
									$Product = &ClassRegistry::init('Product');
									foreach($this->data['details'] as $detail){
										$detail['id']=null;
										$detail['transaction_id']=$transaction_id;
										$detail['soh_quantity'] = $detail['quantity'];
										$type = $this->data['header']['type'];
										switch($this->data['header']['entity_type']){
											case 'customer':
												if($type=='tradein') $type = 'TIN'; //Trade In
												else if($type=='return') $type = 'RFC'; //Return From Customer
												$Product->updateInventory(array($detail),1,$type);
											break;
											case 'supplier':
												if($type=='return') $type = 'R2S'; //Return To Supplier
												$Product->updateInventory(array($detail),-1,$type);
											break;
										}
										$Endpoint->TransactionDetail->save($detail);
									}
									foreach($this->data['payments'] as $payment){
										$payment['id']=null;
										$payment['transaction_id']=$transaction_id;
										$Endpoint->TransactionPayment->save($payment);
										$transac_date = $this->data['header']['transac_date'];
										if($payment['payment_type']=='CHRG'){
											switch($this->data['header']['entity_type']){
												case 'customer':
													$CustomerLedger = &ClassRegistry::init('CustomerLedger');
													$entry = array('CustomerLedger'=>array(
																'id'=>null,
																'customer_id'=>$this->data['header']['entity']['id'],
																'ref_no'=>$this->data['header']['type'].'-'.$transac_date,
																'particulars'=>$payment['detail'],
																'amount'=>$payment['amount'],
																'flag'=>'d',
																'timestamp'=>$this->data['header']['timestamp'],
															));
													$CustomerLedger->save($entry);
												break;
												case 'supplier':
													$SupplierLedger = &ClassRegistry::init('SupplierLedger');
													$entry = array('SupplierLedger'=>array(
																'id'=>null,
																'supplier_id'=>$this->data['header']['entity']['id'],
																'ref_no'=>$this->data['header']['type'].'-'.$transac_date,
																'particulars'=>$payment['detail'],
																'amount'=>$payment['amount'],
																'flag'=>'d',
																'timestamp'=>$this->data['header']['timestamp'],
															));
													$SupplierLedger->save($entry);
												break;
											}
										}else if($payment['payment_type']=='CASH'){
											$CashFlow =  &ClassRegistry::init('CashFlow');
											$entry = array('CashFlow'=>array(
															'id'=>null,
															'particulars'=>$this->data['header']['type'].'-'.$transac_date,
															'amount'=>$this->data['header']['amount'],
															'flag'=>'d', //Expense
															'timestamp'=>$this->data['header']['timestamp']

														));
											$CashFlow->save($entry);
										}
									}
								break;
							}
						}else if($__Class=='Product'){
							if(isset($this->data['adjusted']['quantity'])){
								if($this->data['adjusted']['quantity']) $this->data['tmp_quantity'] = null;
							}
							if(isset($this->data['adjusted']['price'])){
								if($this->data['adjusted']['price']) $this->data['tmp_srp'] = null;
							}
						}else if($__Class=='User'){
							if(isset($this->data['password'])) $this->data['password']  = md5($this->data['password']);
							$Endpoint->save($this->data);
							$user_id = $this->data['id'] = $Endpoint->id;
							if(isset($this->data['access'])) $Endpoint->updateAccess($this->data['access'],$user_id);
							else $Endpoint->revokeAccess($user_id);
						}
						$Endpoint->save($this->data);
						$this->data['id'] = $Endpoint->id;
					break;
					case 'delete':
						if(isset($this->data['id'])){
							$Endpoint->delete($this->data['id']);
						}
					break;
					case 'archive':
						if(isset($this->data['id'])){
							$object = array('id'=>$this->data['id'],'status'=>'archive');
							$Endpoint->save($object);
						}
					break;
					case 'activate':
						if(isset($this->data['id'])){
							$object = array('id'=>$this->data['id'],'status'=>'active');
							$Endpoint->save($object);
						}
					break;
					case 'update':
						if($__Class=='Product'){
							$adjustment = array('id'=>null,'product_id'=>$this->data['id'],'tmp_quantity'=>$this->data['tmp_quantity']);
							$InventoryAdjustment =  &ClassRegistry::init('InventoryAdjustment');
							$InventoryAdjustment->save($adjustment);
							$Endpoint->save($this->data);
						}
					break;
					case 'post':
						if($__Class=='Customer' || $__Class=='Supplier'){
							$last_bill = $this->data['last_bill'];
							$current_month = (int) date('m', strtotime("this month"));
							if($last_bill){
								if($current_month!=$last_bill){
									$last_bill = $current_month;
								}
							}
							else $last_bill = 7;
							$this->data['last_bill'] = $last_bill;
							$Endpoint->save($this->data);
						}
					break;
					case 'cancel':
						if($__Class=='Transaction'){
							
							$trnx = $Endpoint->findById($this->data['header']['id']);
							$this->data['header']['status']='cancelled';
							$this->data['header']['commission']=$trnx['Transaction']['commission'];
							$this->data['header']['tax']=$trnx['Transaction']['tax'];
							$this->data['header']['discount']=$trnx['Transaction']['discount'];
							$timestamp = date('Y-m-d H:i:s', time());
							$Endpoint->save($this->data['header']);
							switch($this->data['header']['type']){
								case 'sales':
									foreach($this->data['details'] as $detail){
										$detail['soh_quantity'] = $detail['quantity'];
										$Endpoint->TransactionDetail->Product->updateInventory(array($detail),1,'CSS');
									}
									foreach($this->data['payments'] as $payment){
										if($payment['payment_type']=='CHRG'){
											switch($this->data['header']['entity_type']){
												case 'customer':
													$CustomerLedger = &ClassRegistry::init('CustomerLedger');
													$entry = array('CustomerLedger'=>array(
																'id'=>null,
																'customer_id'=>$this->data['header']['entity']['id'],
																'ref_no'=>$this->data['header']['type'].'-'.$this->data['header']['ref_no'],
																'particulars'=>'Cancel Transaction',
																'amount'=>$payment['amount'],
																'flag'=>'d',
																'timestamp'=>$timestamp
															));
													$CustomerLedger->save($entry);
												break;
											}
										}
									}
								break;
								case 'deliveries':
									foreach($this->data['details'] as $detail){
										$detail['soh_quantity'] = $detail['quantity'];
										$detail['capital'] = $detail['price'];
										$ref_no = $this->data['header']['ref_no'];
										$Endpoint->TransactionDetail->Product->updateInventory(array($detail),-1,'CDL');
										$Endpoint->TransactionDetail->Product->updatePricing(array($detail),'canceldelivery',$ref_no);
									}
									foreach($this->data['payments'] as $payment){
										if($payment['payment_type']=='CHRG'){
											switch($this->data['header']['entity_type']){
												case 'supplier':
													$SupplierLedger = &ClassRegistry::init('SupplierLedger');
													$entry = array('SupplierLedger'=>array(
																'id'=>null,
																'supplier_id'=>$this->data['header']['entity']['id'],
																'ref_no'=>$this->data['header']['type'].'-'.$ref_no,
																'particulars'=>'Cancel Transaction',
																'amount'=>$payment['amount'],
																'flag'=>'d',
																'timestamp'=>$timestamp
															));
													$SupplierLedger->save($entry);
												break;
											}
										}
									}
								break;
								case 'tradein': case 'return':
									foreach($this->data['details'] as $detail){
										$detail['soh_quantity'] = $detail['quantity'];
										$type = $this->data['header']['type'];
										switch($this->data['header']['entity_type']){
											case 'customer':
												if($type=='tradein') $type = 'CTI'; //Trade In
												else if($type=='return') $type = 'CRC'; //Return From Customer
												$Endpoint->TransactionDetail->Product->updateInventory(array($detail),-1,$type);
											break;
											case 'supplier':
												if($type=='return') $type = 'CRS'; //Return To Supplier
												$Endpoint->TransactionDetail->Product->updateInventory(array($detail),1,$type);
											break;
										}
									}
									foreach($this->data['payments'] as $payment){
										if($payment['payment_type']=='CHRG'){
											switch($this->data['header']['entity_type']){
												case 'customer':
													$CustomerLedger = &ClassRegistry::init('CustomerLedger');
													$entry = array('CustomerLedger'=>array(
																'id'=>null,
																'customer_id'=>$this->data['header']['entity']['id'],
																'ref_no'=>$this->data['header']['type'].'-'.$this->data['header']['ref_no'],
																'particulars'=>'Cancel Transaction',
																'amount'=>$payment['amount'],
																'flag'=>'c',
																'timestamp'=>$timestamp
															));
													$CustomerLedger->save($entry);
												break;
												case 'supplier':
													$SupplierLedger = &ClassRegistry::init('SupplierLedger');
													$entry = array('SupplierLedger'=>array(
																'id'=>null,
																'supplier_id'=>$this->data['header']['entity']['id'],
																'ref_no'=>$this->data['header']['type'].'-'.$this->data['header']['ref_no'],
																'particulars'=>'Cancel Transaction',
																'amount'=>$payment['amount'],
																'flag'=>'c',
																'timestamp'=>$timestamp
															));
													$SupplierLedger->save($entry);
												break;
											}
										}
									}
								break;
							}
						}
					break;
				}
				$data = $this->data;
			}
		}else if($this->RequestHandler->isPut()){
			if(!empty($_POST['data'])){
				$this->data =  $_POST['data'];
				$Endpoint->save($this->data);
				$data = array();
			}
		}else if($this->RequestHandler->isDelete()){
		
		}

		$response =  array();
		$response['status'] = '00';
		$response['message'] = '00';		
		$response['response'] =array('meta'=>$meta,'data'=>$data);
		$blacklist = array('Customer','Supplier','id','TransactionDetail','TransactionPayment','customer_id','supplier_id','entity_id','flag','created');
		switch($format){
			case 'csv':
			case 'pdf':
				$cols = array(array());
				$rows = array();

				foreach($data[0] as $key=>$value){
					if(($key=='Customer'||$key=='Supplier')&&!in_array('Transactee',$cols[0])){
						array_push($cols[0],'Transactee');
					}
					if($key=='TransactionDetail'){
						array_push($cols[0],'Product');
						array_push($cols[0],'Qty');
						array_push($cols[0],'Price');
					}
					if($key=='TransactionPayment'){
						array_push($cols[0],'Payment');
						array_push($cols[0],'Cash Amount');
						array_push($cols[0],'Charge Amount');
					}
					if($key=='flag' && ($__Class=='CustomerLedger'||$__Class=='SupplierLedger')){
						array_push($cols[0],'Debit');
						array_push($cols[0],'Credit');						
					}
					if($key=='created'){
						array_push($cols[0],'Timestamp');
					}
					if(!in_array($key,$blacklist)&&!($key=='amount'&& in_array('flag',$export)))
						array_push($cols[0],Inflector::humanize($key));
				}
				$totalAmount = 0;
				$totalCancelled = 0;
				foreach($data as $index=>$datum){
					$row = array();
					$buffer = null;
					$items = null;
					foreach($datum as $key=>$value){
						if($key=='Customer'||$key=='Supplier'){
							$allow=$__Class=='Transaction'?strtolower($key)==$datum['entity_type']:true;
							if(isset($value['name'])){
								if($value['name']&&$allow)
									array_push($row,$value['name']);
							}
						}else{
							if($key=='TransactionDetail'){
								if(!count($buffer)){
									$buffer=array();
									for($bi=0;$bi<count($row);$bi++){
										array_push($buffer,'  ');
									}
								}
								if(is_array($value)&&$format!='pdf'){
									$items = array();
									foreach($value as $k=>$v){
										if(isset($v['Product'])){
											if(isset($v['Product']['display_title'])){
												$item = array('product'=>$v['Product']['display_title'],'quantity'=>$v['quantity'],'amount'=>$v['amount']);
												array_push($items,$item);
											}
										}
									}
									if(isset($items[0])){
										array_push($row,$items[0]['product']);
										array_push($row,$items[0]['quantity']);
										array_push($row,$items[0]['amount']);
									}else{
										array_push($row,' ');
										array_push($row,' ');
										array_push($row,' ');
									}
								}
							}else if($key=='TransactionPayment'){
								if(is_array($value)){
									$payment = array();
									$cash = null;
									$charge = null;
									foreach($value as $k=>$v){
										if($v['payment_type']=='CASH'){
											array_push($payment,$v['payment_type'].'/'.$v['detail'].'/'.$v['amount']);
											$cash = $v['amount'];
										}else if($v['payment_type']=='CHRG'){
											array_push($payment,$v['payment_type'].'/'.$v['detail'].'/'.$v['amount']);
											$charge = $v['amount'];
											if($datum['type']=='return') $charge = -$charge;
											if($datum['status']=='cancelled') $charge = 0;
										}
										else array_push($payment,$v['payment_type'].'/'.$v['detail'].'/'.$v['amount']);
									}
									array_push($row,implode(',',$payment));
									if($cash) array_push($row,$cash);
									if($charge){
										$totalAmount += $charge;
										array_push($row,'');
										array_push($row,$charge);
									} 
								}
							}else if($key=='amount'&& in_array('flag',$export)){
								if($__Class=='CustomerLedger'||$__Class=='SupplierLedger'){
									switch($datum['flag']){
										case 'd':
											array_push($row,$datum['amount']);
											array_push($row,' ');
										break;
										case 'c':
											array_push($row,' ');
											array_push($row,$datum['amount']);
										break;
									}
								}
								
							}
							else if($key!='id'&&$key!='customer_id'&&$key!='supplier_id'&&$key!='entity_id'&&$key!='flag') array_push($row,$value);
							if($key=='amount'){
								 if(!$soa) $totalAmount +=$value;
								 if(isset($datum['status'])){
									if($datum['status']=='cancelled'){
										$totalCancelled +=$value;
									}
								}
							}
							
						}
						
					}
					array_push($rows,$row);
					if(count($buffer)&&$format!='pdf'){
						foreach($items as $ii=>$io){
							if($ii>0){
								$row = array_merge($buffer,	array($io['product'],$io['quantity'],$io['amount']));
								array_push($rows,$row);
							}
						}
					}
				}
				if($totalCancelled>0){
					$row = array();
					array_push($rows,$row);
					if($soa) $totalAmount +=$totalCancelled;
					$row = array(Inflector::humanize('gross_total'),$totalAmount);
						   array_push($rows,$row);
					$row = array(Inflector::humanize('total_cancelled'),$totalCancelled);
						   array_push($rows,$row);
					$netTotal =  $totalAmount - $totalCancelled;
					$row = array(Inflector::humanize('net_total'),$netTotal);
						   array_push($rows,$row);
				}else{
					$netTotal =  $totalAmount;
					$row = array(Inflector::humanize('total_amount'),$totalAmount);
					array_push($rows,$row);
				}
				$row = array();
				array_push($rows,$row);
				
				$filename = $__Class;
				if($__Class=='Transaction' && $soa){
					$class = Inflector::classify($data[0]['entity_type']);
					$Ledger = &ClassRegistry::init($class.'Ledger');
					$Entity = &ClassRegistry::init($class);
					$entity  = $Entity->findById($data[0]['entity_id']);
					$row = array(Inflector::humanize('begin_balance'),$entity[$class]['begin_balance']);
					array_push($rows,$row);
					$bill_month = 'this';
					$current_month = (int) date('m', strtotime("this month"));
					if($last_bill &&  $last_bill == $current_month) $bill_month = "this";
					foreach($bill_detail = $Ledger->getBalances($entity[$class]['begin_balance'],$entity[$class]['id'],$bill_month) as $key=>$value){						
						$row = array(Inflector::humanize($key),$value);
						array_push($rows,$row);
					}
					$filename = $entity[$class]['name'].'- SOA';
					$row = array(Inflector::humanize('Adjustments'),$totalAmount);
				}
				if(isset($filter['type']) && !$soa){
					$filename = Inflector::Humanize($filter['type']) . ' '. $filename;
				}				
				if(isset($filter['coverage'])){
					switch($filter['coverage']){
						case 'today':
							$filename .= ' - ' .$today;
						break;
						case '7D':
							$filename .= ' - ' .$lastWeek . ' - ' .$today;
						break;
						case '30D':
							$filename .= ' - ' .$lastMonth . ' - ' .$today;
						break;
						case 'SOA':
							$filename .= ' - ' .$firstDay . ' - ' .$lastDay;
						break;
					}
				}
				$filename = str_replace(',','',$filename);
				if($format=='csv'){
					$this->header('Content-Type: text/csv; charset=utf-8');
					$this->header('Content-Disposition: attachment; filename='.$filename.'.csv');
					$output = fopen('php://output', 'w');
					foreach(array_merge($cols,$rows) as $d){
						fputcsv($output, $d);
					}
					exit;
				}
				if($format=='pdf'){
					App::import('Vendor','soa');
					$soa =  new SOA();
					$User = &ClassRegistry::init('User');
					$user = $User->me()['User'];
					$due_month = "this";
					$info=array('account'=>$entity[$class]['name'],
								'detail'=>$bill_detail,
								'total'=>number_format($totalAmount,2,'.',','),
								'date'=>date('F d, Y',strtotime("today")),
								'due_date'=>date('F d, Y',strtotime("last day of $due_month  month")),
								'user'=>$user['full_name'],
								);
					$details=array();
					foreach($rows as $line=>$row){
						if(count($row)>2){
							$detail = array();
							$detail['no']=$line+1;
							$detail['date']=date('M d, Y',strtotime($row[0]));
							$detail['details']=$row[2]=='cancelled'?'Cancelled':explode('/',$row[7])[1];
							$amount = $row[2]=='cancelled'?$row[4]:isset($row[9])?$row[9]:$row[8];

							$detail['amount']=array('text'=>number_format(abs($amount),2,'.',','),'value'=>$amount);
							if($row[1]=='return'){
								$detail['details'] = 'Return:'. $detail['details'];
							}
							if($amount<0){
								$detail['amount']['text']= '('.$detail['amount']['text'].')';
							}
							array_push($details,$detail);
						}
					}
					$soa->info($info);
					$soa->details($details,$info);
					$soa->footnote($info);
					$soa->output();
				}
			break;
			case 'json':
			default:
				$this->header('Content-Type: application/json');
				echo json_encode($response,JSON_NUMERIC_CHECK  | JSON_PRETTY_PRINT);exit;
			break;
		}		
	}
}
