<?php
class OrderDetailsController extends AppController {

	var $name = 'OrderDetails';

	function index() {
		$this->OrderDetail->recursive = 0;
		$this->set('orderDetails', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid order detail', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('orderDetail', $this->OrderDetail->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->OrderDetail->create();
			if ($this->OrderDetail->save($this->data)) {
				$this->Session->setFlash(__('The order detail has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The order detail could not be saved. Please, try again.', true));
			}
		}
		$orders = $this->OrderDetail->Order->find('list');
		$products = $this->OrderDetail->Product->find('list');
		$this->set(compact('orders', 'products'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid order detail', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->OrderDetail->save($this->data)) {
				$this->Session->setFlash(__('The order detail has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The order detail could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->OrderDetail->read(null, $id);
		}
		$orders = $this->OrderDetail->Order->find('list');
		$products = $this->OrderDetail->Product->find('list');
		$this->set(compact('orders', 'products'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for order detail', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->OrderDetail->delete($id)) {
			$this->Session->setFlash(__('Order detail deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Order detail was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
