<?php
class DeliveryDetailsController extends AppController {

	var $name = 'DeliveryDetails';

	function index() {
		$this->DeliveryDetail->recursive = 0;
		$this->set('deliveryDetails', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid delivery detail', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('deliveryDetail', $this->DeliveryDetail->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->DeliveryDetail->create();
			if ($this->DeliveryDetail->save($this->data)) {
				$this->Session->setFlash(__('The delivery detail has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The delivery detail could not be saved. Please, try again.', true));
			}
		}
		$products = $this->DeliveryDetail->Product->find('list');
		$this->set(compact('products'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid delivery detail', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->DeliveryDetail->save($this->data)) {
				$this->Session->setFlash(__('The delivery detail has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The delivery detail could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->DeliveryDetail->read(null, $id);
		}
		$products = $this->DeliveryDetail->Product->find('list');
		$this->set(compact('products'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for delivery detail', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->DeliveryDetail->delete($id)) {
			$this->Session->setFlash(__('Delivery detail deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Delivery detail was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
