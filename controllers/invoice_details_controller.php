<?php
class InvoiceDetailsController extends AppController {

	var $name = 'InvoiceDetails';

	function index() {
		$this->InvoiceDetail->recursive = 0;
		$this->set('invoiceDetails', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid invoice detail', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('invoiceDetail', $this->InvoiceDetail->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->InvoiceDetail->create();
			if ($this->InvoiceDetail->save($this->data)) {
				$this->Session->setFlash(__('The invoice detail has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The invoice detail could not be saved. Please, try again.', true));
			}
		}
		$invoices = $this->InvoiceDetail->Invoice->find('list');
		$products = $this->InvoiceDetail->Product->find('list');
		$this->set(compact('invoices', 'products'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid invoice detail', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->InvoiceDetail->save($this->data)) {
				$this->Session->setFlash(__('The invoice detail has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The invoice detail could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->InvoiceDetail->read(null, $id);
		}
		$invoices = $this->InvoiceDetail->Invoice->find('list');
		$products = $this->InvoiceDetail->Product->find('list');
		$this->set(compact('invoices', 'products'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for invoice detail', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->InvoiceDetail->delete($id)) {
			$this->Session->setFlash(__('Invoice detail deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Invoice detail was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
