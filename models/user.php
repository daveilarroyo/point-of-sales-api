<?php
class User extends AppModel {
	var $name = 'User';
	var $virtualFields = array('full_name'=>"CONCAT(first_name,' ',last_name)");
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	 var $hasAndBelongsToMany = array(
        'Module' =>
            array(
                'className'              => 'Module',
                'joinTable'              => 'module_users',
                'foreignKey'             => 'user_id',
                'associationForeignKey'  => 'module_id',
            )
    );
	function authenticate($data){
		//$data['password']  = md5($data['password']);
		unset($data['password']);
		return  $this->find('first',array('conditions'=>$data));
	}
	function me(){
		$this->recursive=0;
		return $this->find('first',array('conditions'=>array('id'=>$_SESSION['USERID']),'fields'=>array('id','first_name','last_name','full_name')));
	}
	function updateAccess($access,$id){
		$this->revokeAccess($id);
		$module_user = array();
		foreach($access as $a){
			array_push($module_user, array('user_id'=>$id,'module_id'=>$a));
		}
		$this->ModuleUser->saveAll($module_user);
	}
	function revokeAccess($id){
		$this->ModuleUser->deleteAll(array('ModuleUser.user_id' => $id), false);
	}
}
