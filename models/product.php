<?php
class Product extends AppModel {
	var $name = 'Product';
	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $actsAs = array('Increment'=>array('incrementFieldName'=>'soh_quantity'));
	var $virtualFields = array('display_title'=>'CONCAT((Product.particular)," ",Product.part_no," ",(Product.description))',
	'code_lord'=>'REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( 
				REPLACE( REPLACE( REPLACE( REPLACE(
					REPLACE(ROUND(capital,0),1,"T")
											,2,"H")
											,3,"A")
											,4,"N")
											,5,"K")
											,6,"S")
											,7,"L")
											,8,"O")
											,9,"R")
											,0,"D")
											');
	var $belongsTo = array(
		'Category' => array(
			'className' => 'Category',
			'foreignKey' => 'category_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	var $hasMany = array(
		'DeliveryDetail' => array(
			'className' => 'DeliveryDetail',
			'foreignKey' => 'product_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'InventoryLog' => array(
			'className' => 'InventoryLog',
			'foreignKey' => 'product_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'PriceLog' => array(
			'className' => 'PriceLog',
			'foreignKey' => 'product_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'InventoryAdjustment' => array(
			'className' => 'InventoryAdjustment',
			'foreignKey' => 'product_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'InvoiceDetail' => array(
			'className' => 'InvoiceDetail',
			'foreignKey' => 'product_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'OrderDetail' => array(
			'className' => 'OrderDetail',
			'foreignKey' => 'product_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
	function updateInventory($items,$flag,$source){
		foreach($items as $item){
			$old_quantity = $this->getQuantity($item['product_id']);
			if($old_quantity){
				$this->doIncrement($item['product_id'],$item['soh_quantity']*$flag);
			}
			$new_quantity = $this->getQuantity($item['product_id']);
			
			$transaction = array('InventoryLog'=>array());
			$transaction['InventoryLog']['id'] = null;
			$transaction['InventoryLog']['product_id'] = $item['product_id'];
			$transaction['InventoryLog']['old_quantity'] = $old_quantity['Product']['soh_quantity'];
			$transaction['InventoryLog']['act_quantity'] = $item['quantity'];
			$transaction['InventoryLog']['new_quantity'] = $new_quantity['Product']['soh_quantity'];
			$transaction['InventoryLog']['source'] =$source;
			
			$this->InventoryLog->save($transaction['InventoryLog']);
		}
	}
	function updatePricing($items,$type=null,$ref_no=null){
		foreach($items as $item){
			$id = $item['product_id'];
			$this->recursive = 0;
			$markup = $this->findById($id, array('id','markup','srp'));
			
			$old_srp = $markup['Product']['srp'];
			$markup = $markup['Product']['markup'];
			
			$srp_fld = 'srp';
			if($type=='tmp'){
				if($old_srp!=$item['price']){
					$srp = $item['price'];
					$srp_fld = 'tmp_srp';
				}else{
					return;
				}
			}else{
				$capital = round($item['capital']);
				$new_srp = $capital + $markup;
				
				$transaction = array('PriceLog'=>array());
				$transaction['PriceLog']['id'] = null;
				$transaction['PriceLog']['product_id'] = $item['product_id'];
				
				if($type=='delivery'){
					if($new_srp<=$old_srp){
						$srp = $old_srp;
						$markup  = $markup  + ($old_srp -  $new_srp);
					}
					else $srp = $new_srp;
					$transaction['PriceLog']['source'] ='DEL';
					$transaction['PriceLog']['ref_no'] =$ref_no;
				}else if($type=='canceldelivery'){
					$delivery =  $this->PriceLog->find('first',array('conditions'=>array('source'=>'DEL','ref_no'=>$ref_no)));

					$new_srp = $delivery['PriceLog']['old_price'];
					if($new_srp){
						$old_srp = $delivery['PriceLog']['new_price'];
						$srp = $new_srp;
						if($new_srp>$old_srp)
							$markup  = $markup  - ($new_srp -  $old_srp);
						$capital = $srp - $markup;
					}else{
						$srp = $old_srp;
					}
					$transaction['PriceLog']['source'] = 'CDL';
					$transaction['PriceLog']['ref_no'] = $ref_no;
				}
				
				$transaction['PriceLog']['old_price'] = $old_srp;
				$transaction['PriceLog']['new_price'] = $new_srp;
				
				if($new_srp!=$old_srp) $this->PriceLog->save($transaction['PriceLog']);
				
			}
			
			$item = array('id'=>$id,$srp_fld=>$srp);
			if($srp_fld!='tmp_srp') $item['capital'] = $capital;
			$item['markup'] = $markup;
			$this->save($item);
		}
	}
}
