<?php
class Invoice extends AppModel {
	var $name = 'Invoice';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $hasMany = array(
		'InvoiceDetail' => array(
			'className' => 'InvoiceDetail',
			'foreignKey' => 'invoice_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'InvoicePayment' => array(
			'className' => 'InvoicePayment',
			'foreignKey' => 'invoice_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
