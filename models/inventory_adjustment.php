<?php
class InventoryAdjustment extends AppModel {
	var $name = 'InventoryAdjustment';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'Product' => array(
			'className' => 'Product',
			'foreignKey' => 'product_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
	function getLastAdjustment($product_id){
		return $this->query("SELECT 
		  inventory_adjustments.product_id,
		  inventory_adjustments.created,
		  inventory_adjustments.tmp_quantity 
		FROM
		  inventory_adjustments 
		  INNER JOIN 
			(SELECT 
			  product_id,
			  MAX(created) AS created 
			FROM
			  inventory_adjustments 
			WHERE product_id = '$product_id') AS last_adjustment 
			ON inventory_adjustments.`product_id` = last_adjustment.product_id 
			AND inventory_adjustments.`created` = last_adjustment.created 
			WHERE inventory_adjustments.product_id = '$product_id'
			");
	}
}
