<?php
class Transaction extends AppModel {
	var $name = 'Transaction';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $hasMany = array(
		'TransactionDetail' => array(
			'className' => 'TransactionDetail',
			'foreignKey' => 'transaction_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'TransactionPayment' => array(
			'className' => 'TransactionPayment',
			'foreignKey' => 'transaction_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
	var $belongsTo = array(
		'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => false,
			'conditions' => array("Transaction.entity_id = Customer.id AND Transaction.entity_type = 'customer'"),
			'fields' => array('Customer.id','Customer.name'),
			'order' => ''
		),
		'Supplier' => array(
			'className' => 'Supplier',
			'foreignKey' => false,
			'conditions' => array("Transaction.entity_id = Supplier.id AND Transaction.entity_type = 'supplier'"),
			'fields' => array('Supplier.id','Supplier.name'),
			'order' => ''
		)
	);

}
