<?php
class InventoryLog extends AppModel {
	var $name = 'InventoryLog';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'Product' => array(
			'className' => 'Product',
			'foreignKey' => 'product_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	function getProductActivity($product_id,$created){
		return $this->query("SELECT SUM(act_quantity) as quantity, source FROM inventory_logs WHERE product_id = '$product_id' AND created > '$created' GROUP BY source");
	}
}
