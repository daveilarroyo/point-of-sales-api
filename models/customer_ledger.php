<?php
class CustomerLedger extends AppModel {
	var $name = 'CustomerLedger';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => 'customer_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	function getBalances($balance,$id,$month='previous'){
		$data = $cond = array();
		$foreign_key = 'customer_id';
		$firstDay = date('Y-m-d', strtotime("first day of $month month"));
		$lastDay = date('Y-m-d', strtotime("last day of $month  month"));
		$cutoff = date('F 1 - d', strtotime("last day of $month  month"));
		$this->recursive=-1;
		$conf = array('fields'=>array('SUM(amount) as amount'));
		$cond[$foreign_key.' ='] = $id;
		$cond['timestamp >='] = $firstDay. ' 00:00:00';
		$cond['timestamp <='] = $lastDay. ' 23:59:59';
		$conf['conditions'] = $cond;
		$conf['conditions']['flag ='] = 'c';
		$data['charges']=$this->find('all',$conf)[0][0]['amount'] + 0;
		$conf['conditions']['flag ='] = 'd';
		$data['payments']=$this->find('all',$conf)[0][0]['amount'] + 0;
		$data['current_balance'] = $balance + $data['charges'] - $data['payments'];
		$data['cutoff'] = $cutoff;
		return $data;
	}
}
