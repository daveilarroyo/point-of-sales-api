<?php
/* Vendors Test cases generated on: 2015-03-03 14:49:05 : 1425394145*/
App::import('Controller', 'Vendors');

class TestVendorsController extends VendorsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class VendorsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.vendor', 'app.order', 'app.order_detail', 'app.product', 'app.category', 'app.delivery_detail', 'app.invoice_detail', 'app.invoice');

	function startTest() {
		$this->Vendors =& new TestVendorsController();
		$this->Vendors->constructClasses();
	}

	function endTest() {
		unset($this->Vendors);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
