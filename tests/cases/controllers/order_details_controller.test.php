<?php
/* OrderDetails Test cases generated on: 2015-03-03 14:49:04 : 1425394144*/
App::import('Controller', 'OrderDetails');

class TestOrderDetailsController extends OrderDetailsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class OrderDetailsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.order_detail', 'app.order', 'app.vendor', 'app.product', 'app.category', 'app.delivery_detail', 'app.invoice_detail', 'app.invoice');

	function startTest() {
		$this->OrderDetails =& new TestOrderDetailsController();
		$this->OrderDetails->constructClasses();
	}

	function endTest() {
		unset($this->OrderDetails);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
