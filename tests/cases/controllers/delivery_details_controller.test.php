<?php
/* DeliveryDetails Test cases generated on: 2015-03-03 14:49:04 : 1425394144*/
App::import('Controller', 'DeliveryDetails');

class TestDeliveryDetailsController extends DeliveryDetailsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class DeliveryDetailsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.delivery_detail', 'app.product', 'app.category', 'app.invoice_detail', 'app.invoice', 'app.order_detail', 'app.order', 'app.vendor');

	function startTest() {
		$this->DeliveryDetails =& new TestDeliveryDetailsController();
		$this->DeliveryDetails->constructClasses();
	}

	function endTest() {
		unset($this->DeliveryDetails);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
