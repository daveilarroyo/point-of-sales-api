<?php
/* InvoiceDetails Test cases generated on: 2015-03-03 14:49:04 : 1425394144*/
App::import('Controller', 'InvoiceDetails');

class TestInvoiceDetailsController extends InvoiceDetailsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class InvoiceDetailsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.invoice_detail', 'app.invoice', 'app.product', 'app.category', 'app.delivery_detail', 'app.order_detail', 'app.order', 'app.vendor');

	function startTest() {
		$this->InvoiceDetails =& new TestInvoiceDetailsController();
		$this->InvoiceDetails->constructClasses();
	}

	function endTest() {
		unset($this->InvoiceDetails);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
