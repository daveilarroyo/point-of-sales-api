<?php
/* Deliveries Test cases generated on: 2015-03-03 14:49:04 : 1425394144*/
App::import('Controller', 'Deliveries');

class TestDeliveriesController extends DeliveriesController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class DeliveriesControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.delivery');

	function startTest() {
		$this->Deliveries =& new TestDeliveriesController();
		$this->Deliveries->constructClasses();
	}

	function endTest() {
		unset($this->Deliveries);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
