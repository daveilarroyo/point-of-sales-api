<?php
/* PriceLog Test cases generated on: 2015-08-06 05:08:56 : 1438830536*/
App::import('Model', 'PriceLog');

class PriceLogTestCase extends CakeTestCase {
	var $fixtures = array('app.price_log', 'app.product', 'app.category', 'app.delivery_detail', 'app.delivery', 'app.delivery_payment', 'app.inventory_log', 'app.inventory_adjustment', 'app.invoice_detail', 'app.invoice', 'app.invoice_payment', 'app.order_detail', 'app.order');

	function startTest() {
		$this->PriceLog =& ClassRegistry::init('PriceLog');
	}

	function endTest() {
		unset($this->PriceLog);
		ClassRegistry::flush();
	}

}
