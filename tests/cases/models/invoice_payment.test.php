<?php
/* InvoicePayment Test cases generated on: 2015-06-12 03:39:25 : 1434080365*/
App::import('Model', 'InvoicePayment');

class InvoicePaymentTestCase extends CakeTestCase {
	var $fixtures = array('app.invoice_payment', 'app.invoice', 'app.invoice_detail', 'app.product', 'app.category', 'app.delivery_detail', 'app.inventory_log', 'app.order_detail', 'app.order', 'app.vendor');

	function startTest() {
		$this->InvoicePayment =& ClassRegistry::init('InvoicePayment');
	}

	function endTest() {
		unset($this->InvoicePayment);
		ClassRegistry::flush();
	}

}
