<?php
/* Invoice Test cases generated on: 2015-06-12 03:39:31 : 1434080371*/
App::import('Model', 'Invoice');

class InvoiceTestCase extends CakeTestCase {
	var $fixtures = array('app.invoice', 'app.invoice_detail', 'app.product', 'app.category', 'app.delivery_detail', 'app.inventory_log', 'app.order_detail', 'app.order', 'app.vendor', 'app.invoice_payment');

	function startTest() {
		$this->Invoice =& ClassRegistry::init('Invoice');
	}

	function endTest() {
		unset($this->Invoice);
		ClassRegistry::flush();
	}

}
