<?php
/* InventoryAdjustment Test cases generated on: 2015-06-14 23:56:35 : 1434326195*/
App::import('Model', 'InventoryAdjustment');

class InventoryAdjustmentTestCase extends CakeTestCase {
	var $fixtures = array('app.inventory_adjustment', 'app.product', 'app.category', 'app.delivery_detail', 'app.inventory_log', 'app.invoice_detail', 'app.invoice', 'app.invoice_payment', 'app.order_detail', 'app.order', 'app.supplier', 'app.delivery');

	function startTest() {
		$this->InventoryAdjustment =& ClassRegistry::init('InventoryAdjustment');
	}

	function endTest() {
		unset($this->InventoryAdjustment);
		ClassRegistry::flush();
	}

}
