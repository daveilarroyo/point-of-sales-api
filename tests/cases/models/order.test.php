<?php
/* Order Test cases generated on: 2015-07-12 07:53:57 : 1436687637*/
App::import('Model', 'Order');

class OrderTestCase extends CakeTestCase {
	var $fixtures = array('app.order', 'app.order_detail', 'app.product', 'app.category', 'app.delivery_detail', 'app.delivery', 'app.delivery_payment', 'app.inventory_log', 'app.inventory_adjustment', 'app.invoice_detail', 'app.invoice', 'app.invoice_payment');

	function startTest() {
		$this->Order =& ClassRegistry::init('Order');
	}

	function endTest() {
		unset($this->Order);
		ClassRegistry::flush();
	}

}
