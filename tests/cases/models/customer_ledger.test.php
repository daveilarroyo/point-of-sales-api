<?php
/* CustomerLedger Test cases generated on: 2015-06-22 00:54:16 : 1434934456*/
App::import('Model', 'CustomerLedger');

class CustomerLedgerTestCase extends CakeTestCase {
	var $fixtures = array('app.customer_ledger', 'app.customer');

	function startTest() {
		$this->CustomerLedger =& ClassRegistry::init('CustomerLedger');
	}

	function endTest() {
		unset($this->CustomerLedger);
		ClassRegistry::flush();
	}

}
