<?php
/* Module Test cases generated on: 2015-08-06 06:02:19 : 1438833739*/
App::import('Model', 'Module');

class ModuleTestCase extends CakeTestCase {
	var $fixtures = array('app.module');

	function startTest() {
		$this->Module =& ClassRegistry::init('Module');
	}

	function endTest() {
		unset($this->Module);
		ClassRegistry::flush();
	}

}
