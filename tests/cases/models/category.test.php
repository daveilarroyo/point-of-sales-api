<?php
/* Category Test cases generated on: 2015-03-03 17:47:53 : 1425404873*/
App::import('Model', 'Category');

class CategoryTestCase extends CakeTestCase {
	var $fixtures = array('app.category', 'app.product', 'app.delivery_detail', 'app.invoice_detail', 'app.invoice', 'app.order_detail', 'app.order', 'app.vendor');

	function startTest() {
		$this->Category =& ClassRegistry::init('Category');
	}

	function endTest() {
		unset($this->Category);
		ClassRegistry::flush();
	}

}
