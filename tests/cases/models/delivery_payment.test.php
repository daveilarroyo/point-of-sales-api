<?php
/* DeliveryPayment Test cases generated on: 2015-07-09 08:49:30 : 1436431770*/
App::import('Model', 'DeliveryPayment');

class DeliveryPaymentTestCase extends CakeTestCase {
	var $fixtures = array('app.delivery_payment', 'app.delivery', 'app.delivery_detail', 'app.product', 'app.category', 'app.inventory_log', 'app.inventory_adjustment', 'app.invoice_detail', 'app.invoice', 'app.invoice_payment', 'app.order_detail', 'app.order', 'app.supplier');

	function startTest() {
		$this->DeliveryPayment =& ClassRegistry::init('DeliveryPayment');
	}

	function endTest() {
		unset($this->DeliveryPayment);
		ClassRegistry::flush();
	}

}
