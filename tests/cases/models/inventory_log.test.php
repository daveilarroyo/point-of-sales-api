<?php
/* InventoryLog Test cases generated on: 2015-03-03 17:47:55 : 1425404875*/
App::import('Model', 'InventoryLog');

class InventoryLogTestCase extends CakeTestCase {
	var $fixtures = array('app.inventory_log', 'app.product', 'app.category', 'app.delivery_detail', 'app.invoice_detail', 'app.invoice', 'app.order_detail', 'app.order', 'app.vendor');

	function startTest() {
		$this->InventoryLog =& ClassRegistry::init('InventoryLog');
	}

	function endTest() {
		unset($this->InventoryLog);
		ClassRegistry::flush();
	}

}
