<?php
/* InvoiceDetail Test cases generated on: 2015-06-12 03:39:37 : 1434080377*/
App::import('Model', 'InvoiceDetail');

class InvoiceDetailTestCase extends CakeTestCase {
	var $fixtures = array('app.invoice_detail', 'app.invoice', 'app.invoice_payment', 'app.product', 'app.category', 'app.delivery_detail', 'app.inventory_log', 'app.order_detail', 'app.order', 'app.vendor');

	function startTest() {
		$this->InvoiceDetail =& ClassRegistry::init('InvoiceDetail');
	}

	function endTest() {
		unset($this->InvoiceDetail);
		ClassRegistry::flush();
	}

}
