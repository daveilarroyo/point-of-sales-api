<?php
/* OrderDetail Test cases generated on: 2015-03-03 17:47:57 : 1425404877*/
App::import('Model', 'OrderDetail');

class OrderDetailTestCase extends CakeTestCase {
	var $fixtures = array('app.order_detail', 'app.order', 'app.vendor', 'app.product', 'app.category', 'app.delivery_detail', 'app.invoice_detail', 'app.invoice');

	function startTest() {
		$this->OrderDetail =& ClassRegistry::init('OrderDetail');
	}

	function endTest() {
		unset($this->OrderDetail);
		ClassRegistry::flush();
	}

}
