<?php
/* SupplierLedger Test cases generated on: 2015-07-08 02:10:44 : 1436321444*/
App::import('Model', 'SupplierLedger');

class SupplierLedgerTestCase extends CakeTestCase {
	var $fixtures = array('app.supplier_ledger', 'app.supplier', 'app.delivery', 'app.order', 'app.order_detail', 'app.product', 'app.category', 'app.delivery_detail', 'app.inventory_log', 'app.inventory_adjustment', 'app.invoice_detail', 'app.invoice', 'app.invoice_payment');

	function startTest() {
		$this->SupplierLedger =& ClassRegistry::init('SupplierLedger');
	}

	function endTest() {
		unset($this->SupplierLedger);
		ClassRegistry::flush();
	}

}
