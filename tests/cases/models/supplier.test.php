<?php
/* Supplier Test cases generated on: 2015-07-12 07:54:18 : 1436687658*/
App::import('Model', 'Supplier');

class SupplierTestCase extends CakeTestCase {
	var $fixtures = array('app.supplier', 'app.supplier_ledger');

	function startTest() {
		$this->Supplier =& ClassRegistry::init('Supplier');
	}

	function endTest() {
		unset($this->Supplier);
		ClassRegistry::flush();
	}

}
