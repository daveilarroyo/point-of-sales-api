<?php
/* Vendor Test cases generated on: 2015-03-03 17:48:00 : 1425404880*/
App::import('Model', 'Vendor');

class VendorTestCase extends CakeTestCase {
	var $fixtures = array('app.vendor', 'app.order', 'app.order_detail', 'app.product', 'app.category', 'app.delivery_detail', 'app.invoice_detail', 'app.invoice');

	function startTest() {
		$this->Vendor =& ClassRegistry::init('Vendor');
	}

	function endTest() {
		unset($this->Vendor);
		ClassRegistry::flush();
	}

}
