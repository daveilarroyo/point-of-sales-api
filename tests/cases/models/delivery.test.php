<?php
/* Delivery Test cases generated on: 2015-07-09 08:49:05 : 1436431745*/
App::import('Model', 'Delivery');

class DeliveryTestCase extends CakeTestCase {
	var $fixtures = array('app.delivery', 'app.delivery_detail', 'app.product', 'app.category', 'app.inventory_log', 'app.inventory_adjustment', 'app.invoice_detail', 'app.invoice', 'app.invoice_payment', 'app.order_detail', 'app.order', 'app.supplier', 'app.delivery_payment');

	function startTest() {
		$this->Delivery =& ClassRegistry::init('Delivery');
	}

	function endTest() {
		unset($this->Delivery);
		ClassRegistry::flush();
	}

}
