<?php
/* DeliveryDetail Test cases generated on: 2015-07-09 08:49:14 : 1436431754*/
App::import('Model', 'DeliveryDetail');

class DeliveryDetailTestCase extends CakeTestCase {
	var $fixtures = array('app.delivery_detail', 'app.delivery', 'app.delivery_payment', 'app.product', 'app.category', 'app.inventory_log', 'app.inventory_adjustment', 'app.invoice_detail', 'app.invoice', 'app.invoice_payment', 'app.order_detail', 'app.order', 'app.supplier');

	function startTest() {
		$this->DeliveryDetail =& ClassRegistry::init('DeliveryDetail');
	}

	function endTest() {
		unset($this->DeliveryDetail);
		ClassRegistry::flush();
	}

}
