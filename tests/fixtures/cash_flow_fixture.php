<?php
/* CashFlow Fixture generated on: 2015-08-31 02:34:35 : 1440988475 */
class CashFlowFixture extends CakeTestFixture {
	var $name = 'CashFlow';

	var $fields = array(
		'id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 36, 'key' => 'primary', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'source' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 15, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'amount' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '10,2'),
		'flag' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 1, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'timestamp' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => '55e3bd3b-e894-4797-8d31-019f53314baa',
			'source' => 'Lorem ipsum d',
			'amount' => 1,
			'flag' => 'Lorem ipsum dolor sit ame',
			'timestamp' => '2015-08-31 02:34:35',
			'created' => '2015-08-31 02:34:35',
			'modified' => '2015-08-31 02:34:35'
		),
	);
}
