<?php
/* InvoicePayment Fixture generated on: 2015-06-12 03:39:25 : 1434080365 */
class InvoicePaymentFixture extends CakeTestFixture {
	var $name = 'InvoicePayment';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'invoice_id' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'payment_type' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 4, 'collate' => 'latin1_swedish_ci', 'comment' => 'CASH, CARD, CHQE, CHRG', 'charset' => 'latin1'),
		'detail' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'amount' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '10,2'),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'invoice_id' => 1,
			'payment_type' => 'Lo',
			'detail' => 'Lorem ipsum dolor sit amet',
			'amount' => 1
		),
	);
}
