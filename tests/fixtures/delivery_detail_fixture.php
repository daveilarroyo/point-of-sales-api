<?php
/* DeliveryDetail Fixture generated on: 2015-07-09 08:49:14 : 1436431754 */
class DeliveryDetailFixture extends CakeTestFixture {
	var $name = 'DeliveryDetail';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'delivery_id' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'product_id' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'price' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '8,2'),
		'quantity' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'amount' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '10,2'),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'delivery_id' => 1,
			'product_id' => 1,
			'price' => 1,
			'quantity' => 1,
			'amount' => 1
		),
	);
}
