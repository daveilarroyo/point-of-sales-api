<?php
/* PriceLog Fixture generated on: 2015-08-06 05:08:56 : 1438830536 */
class PriceLogFixture extends CakeTestFixture {
	var $name = 'PriceLog';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'product_id' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'old_price' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '10,2'),
		'new_price' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '10,2'),
		'source' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 3, 'collate' => 'latin1_swedish_ci', 'comment' => 'INV-Invoice, DEL-Delivery', 'charset' => 'latin1'),
		'ref_no' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'product_id' => 1,
			'old_price' => 1,
			'new_price' => 1,
			'source' => 'L',
			'ref_no' => 1,
			'created' => '2015-08-06 05:08:56'
		),
	);
}
