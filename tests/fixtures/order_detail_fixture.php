<?php
/* OrderDetail Fixture generated on: 2015-03-03 17:47:57 : 1425404877 */
class OrderDetailFixture extends CakeTestFixture {
	var $name = 'OrderDetail';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'order_id' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'product_id' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'quantity' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'price' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '8,2'),
		'amount' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '8,2'),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'order_id' => 1,
			'product_id' => 1,
			'quantity' => 1,
			'price' => 1,
			'amount' => 1
		),
	);
}
