<?php
/* Order Fixture generated on: 2015-07-12 07:53:57 : 1436687637 */
class OrderFixture extends CakeTestFixture {
	var $name = 'Order';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'supplier' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'order_date' => array('type' => 'date', 'null' => true, 'default' => NULL),
		'delivery_date' => array('type' => 'date', 'null' => true, 'default' => NULL),
		'total' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '10,2'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'supplier' => 'Lorem ipsum dolor sit amet',
			'order_date' => '2015-07-12',
			'delivery_date' => '2015-07-12',
			'total' => 1,
			'created' => '2015-07-12 07:53:57'
		),
	);
}
