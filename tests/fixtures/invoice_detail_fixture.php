<?php
/* InvoiceDetail Fixture generated on: 2015-06-12 03:39:37 : 1434080377 */
class InvoiceDetailFixture extends CakeTestFixture {
	var $name = 'InvoiceDetail';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'invoice_id' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'product_id' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'quantity' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'price' => array('type' => 'float', 'null' => false, 'default' => NULL, 'length' => '8,2'),
		'amount' => array('type' => 'float', 'null' => false, 'default' => NULL, 'length' => '10,2'),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'invoice_id' => 1,
			'product_id' => 1,
			'quantity' => 1,
			'price' => 1,
			'amount' => 1
		),
	);
}
