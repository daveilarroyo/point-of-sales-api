<?php
/* SupplierLedger Fixture generated on: 2015-07-08 02:10:44 : 1436321444 */
class SupplierLedgerFixture extends CakeTestFixture {
	var $name = 'SupplierLedger';

	var $fields = array(
		'id' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 36, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1', 'key' => 'primary'),
		'supplier_id' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'ref_no' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 20, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'particulars' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 100, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'amount' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '10,2'),
		'flag' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 1, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'indexes' => array(),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => '559c86a4-c520-4dc9-bb84-042c2b06e617',
			'supplier_id' => 1,
			'ref_no' => 'Lorem ipsum dolor ',
			'particulars' => 'Lorem ipsum dolor sit amet',
			'amount' => 1,
			'flag' => 'Lorem ipsum dolor sit ame',
			'created' => '2015-07-08 02:10:44',
			'modified' => '2015-07-08 02:10:44'
		),
	);
}
