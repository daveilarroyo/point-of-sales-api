<?php
/* InventoryLog Fixture generated on: 2015-03-03 17:47:55 : 1425404875 */
class InventoryLogFixture extends CakeTestFixture {
	var $name = 'InventoryLog';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'product_id' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'old_quantity' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'act_quantity' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'new_quantity' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'source' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 3, 'collate' => 'latin1_swedish_ci', 'comment' => 'INV-Invoice, DEL-Delivery', 'charset' => 'latin1'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'product_id' => 1,
			'old_quantity' => 1,
			'act_quantity' => 1,
			'new_quantity' => 1,
			'source' => 'L',
			'created' => '2015-03-03 17:47:55'
		),
	);
}
