<?php
/* Invoice Fixture generated on: 2015-06-12 03:39:30 : 1434080370 */
class InvoiceFixture extends CakeTestFixture {
	var $name = 'Invoice';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'invoice_date' => array('type' => 'date', 'null' => true, 'default' => NULL),
		'customer' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 80, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'total' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '10,2'),
		'commission' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '10,2'),
		'discount' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '10,2'),
		'tax' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '10,2'),
		'created' => array('type' => 'date', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'invoice_date' => '2015-06-12',
			'customer' => 'Lorem ipsum dolor sit amet',
			'total' => 1,
			'commission' => 1,
			'discount' => 1,
			'tax' => 1,
			'created' => '2015-06-12'
		),
	);
}
