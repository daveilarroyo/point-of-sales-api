<?php
/* Transaction Fixture generated on: 2015-06-12 03:39:46 : 1434080386 */
class TransactionFixture extends CakeTestFixture {
	var $name = 'Transaction';

	var $fields = array(
		'id' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 36, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1', 'key' => 'primary'),
		'type' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 5, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'entity_type' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 1, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'entity_id' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'ref_no' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 15, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'flag' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 1, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'commission' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '10,2'),
		'discount' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '10,2'),
		'tax' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '10,2'),
		'indexes' => array(),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => '557a5482-b200-41ec-8dac-15702b06e617',
			'type' => 'Lor',
			'entity_type' => 'Lorem ipsum dolor sit ame',
			'entity_id' => 1,
			'ref_no' => 'Lorem ipsum d',
			'flag' => 'Lorem ipsum dolor sit ame',
			'commission' => 1,
			'discount' => 1,
			'tax' => 1
		),
	);
}
