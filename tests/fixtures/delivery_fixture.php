<?php
/* Delivery Fixture generated on: 2015-07-09 08:49:04 : 1436431744 */
class DeliveryFixture extends CakeTestFixture {
	var $name = 'Delivery';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'delivery_date' => array('type' => 'date', 'null' => true, 'default' => NULL),
		'supplier' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'ref_no' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'source' => array('type' => 'string', 'null' => true, 'default' => 'delivery', 'length' => 10, 'collate' => 'latin1_swedish_ci', 'comment' => 'return,order,delivery', 'charset' => 'latin1'),
		'total' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '10,2'),
		'discount' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '10,2'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'delivery_date' => '2015-07-09',
			'supplier' => 'Lorem ipsum dolor sit amet',
			'ref_no' => 1,
			'source' => 'Lorem ip',
			'total' => 1,
			'discount' => 1,
			'created' => '2015-07-09 08:49:04'
		),
	);
}
