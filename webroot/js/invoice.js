jndr.controller('InvoiceController',function($scope,api){
		 $scope.products = {};
		 $scope.selectedProduct = null;
		 $scope.items = [];
		 $scope.total = 0;
		 //Public functions
		 $scope.addItem = function(){
				var product = getSelectedProduct();
				var item = {};
				item.product_id = product.id;
				item.particular = product.particular;
				item.srp = product.srp;
				item.quantity = $scope.quantity;
				item.amount =item.srp*item.quantity;
				item.stamp = Math.random();
				$scope.items.push(item);
				$scope.quantity= null;
				updateTotal();
		 }
		 $scope.removeItem = function(index){
			$scope.items.splice(index,1);
			updateTotal();
		 }
		 //Call loadRemoteData to get list of products
		 loadRemoteData();
		 //Private functions
		 function updateTotal(){
			 angular.forEach($scope.items,function(item){
					$scope.total += item.amount;
			});
			$scope.total =  $scope.total;
		 }
		 function getSelectedProduct(){
			 return $scope.products[$scope.selectedProduct];
		 }
		 function applyRemoteData( newProducts ) {
			console.log(newProducts);
			 for(index in newProducts){
				 var product = newProducts[index]['Product']
				 $scope.products[product.id] = product;
			 }
			 console.log($scope.products);
         }
		function loadRemoteData() {
			api.getProducts().then(
					function( products ) {
						applyRemoteData( products );
					}
				);
		}
	
});